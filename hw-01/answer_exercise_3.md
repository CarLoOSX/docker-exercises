3. Crea un contenedor con las siguientes especificaciones: 	
    a. Utilizar la imagen base NGINX haciendo uso de la versión 1.19.3	
    b. Al acceder a la URL localhost:8080/index.html aparecerá el mensaje 
	    HOMEWORK 1	c. Persistir el fichero index.html en un volumen llamado static_content 

¿Qué comandos has utilizado? 

Para empezar con el punto a y b he creado un Dockerfile para modificar la imagen de NGINX:1.19.3.
Y he usado las siguientes instrucciones para copiar un fichero índex local en la imagen y ya tener una imagen con ese index.
FROM nginx:1.19.3
COPY ./index.html /usr/share/nginx/html/
#RUN touch /usr/share/nginx/html/index.html
#RUN echo "HOMEWORK 1" > /usr/share/nginx/html/index.html
El RUN comentado es porque pensé en crear directamente el fichero en build time, pero vi que era mas cómodo hacer un COPY.

Una vez listo he lanzado el comando Build para crear la imagen a partir de DockerFile y he le llamado nginxtest:1.0 con el siguiente comando:
docker build --tag nginxtest:1.0 .

Una vez creada la imagen con mi fichero index.html he creado un nuevo contenedor llamado  nginxtest1container a partir de esta y he utilizado el comando -d para que los procesos del contenedor corran en segundo plano y - p para indicar por que puerto host quiero acceder al servicio que esta desplegado en el puerto 80 del conainer:
docker run --name nginxtest1container -d -p 8080:80 nginxtest:1.0                                                                                            

Ahora hacemos la comprobación para ver que hemos acabado los apartados a y b.
docker ps   
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
17b3ba526de2        nginxtest:1.0       "/docker-entrypoint.…"   2 minutes ago       Up 2 minutes        0.0.0.0:8080->80/tcp   nginxtest1container
Entramos por terminal para comprobar que el contenido es correcto.
Y terminamos de comprobar:
Apartado b :
He entendido 2 posibles opciones:
Simplemente persistir index html en un volumen llamado static_content para que en un futuro por ejemplo puedan acceder otros containers.
Montar index.html en un volumen para poderlo modificar desde la maquina host y luego refrescar la pagina en el puerto 8080 para ver el nuevo contenido.

Para la opción 1 lo que he hecho es directamente persistir el index.html container en un volumen llamado static_content de esta manera
FROM nginx:1.19.3
RUN mkdir /static_content  
COPY ./index.html /static_content
VOLUME /static_content

Comprobamos de nuevo una vez creamos y lanzamos el container :
docker run --name nginxtest1.1container -d -p 8080:80 nginxtest:1.1
5373734c56ebfa0bf5ff5e93670a5fe6f7f5cd6461cd637457fcbded18ac248b

Si entramos con bash veremos lo siguiente :
root@b5e307d8e689:/static_content# pwd 
/static_content
root@b5e307d8e689:/static_content# ls
index.html
root@b5e307d8e689:/static_content# 

Para el apartado 2 es algo mas sencillo directamente desde el CLI podemos lanzarlo :

docker run --name nginxtest1.1opcionbcontainer -v /Users/carloosx/Documents/MDAS/PHD/Ejercicios/Ex4/index.html:/usr/share/nginx/html/index.html -p 8080:80 -d nginxtest:1.1
79e7354eb8bf56824f0a4f5ef1a902a9668416df4be6a7b71cfac71c28eb045e

—name : nombre del container 
-v lo que quiero mapear y donde mapearlo a través del volumen
-p puerto a exponer
-d en background 


