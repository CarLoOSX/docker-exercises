4. Crea una imagen docker a partir de un Dockerfile.

 Esta aplicación expondrá un servicio en el puerto 8080 y se deberá hacer uso de la instrucción HEALTHCHECK para comprobar si la aplicación está ofreciendo el servicio o por si el contrario existe un problema. 
El healthcheck deberá parametrizarse con la siguiente configuración: 
La prueba se realizará cada 45 segundos 
Por cada prueba realizada, se esperará que la aplicación responda en menos de 5 segundos. Si tras 5 segundos no se obtiene respuesta, se considera que la prueba habrá fallado 
Ajustar el tiempo de espera de la primera prueba (Ejemplo: Si la aplicación del contenedor tarda en iniciarse 10s, configurar el parámetro a 15s) 
El número de reintentos será 2. Si fallan dos pruebas consecutivas, el contenedor deberá cambiar al estado “unhealthy”) 
Para hacer este ejercicio he hecho lo siguiente :
Lo primero es que en vez de hacer un curl por ejemplo al 8080 lo que voy a hacer es comprobar el servicio de Nginx ya que este es el que me dirá si Nginx esta corriendo correctamente.
carloosx  …  PHD  Ejercicios  Ex4  docker exec -it 79e7354eb8bf bash
root@79e7354eb8bf:/# service nginx status
[ ok ] nginx is running.
Aqui he cometido un error ya que al crear el Dockerfile he puesto lo siguiente:
HEALTHCHECK --interval=45s --timeout=5s --start-period=15s --retries=2 CMD ["service","service", "nginx", “status"]
Y al hacer un container y mirar me aparece unhealthy:
Esto se debe a que el servicio de HEALTHCHECK espera un 0 en caso de éxito o un 1 en caso de que no funcione correctamente.

Para poder adaptarlo y comprobar la respuesta de Nginx status, lo que he hecho es crear un script que si aparece que esta running devuelva un 0 y sino un 1.

Asi que he hecho el siguiente script:

#!/bin/bash

if service nginx status; then
    exit 0
else
    exit 1
fi

Lo he añadido a docker y he dejado el DockerFile asi:

FROM nginx:1.19.3
COPY healthchecknginx.sh /
HEALTHCHECK --interval=45s --timeout=5s --start-period=15s --retries=2 CMD ["sh","/healthchecknginx.sh"]



Pues de nuevo hacemos un container con esa nueva imagen y esperamos para comprobar healthcheck :
Efectivamente ahora si podemos comprobar que nuestro Nginx esta healthy 
Justifico, he preferido hacerla de esta manera ya que si lanzamos un servicio de ngninx lo que mas me interesa es que ngninx este corriendo. Si tuviera que comprobar que por ejemplo apareciera cierto texto en el html del 8080 lo hubiera hecho igual con un script pero usando un curl por ejemplo y buscando cierta cadena.
