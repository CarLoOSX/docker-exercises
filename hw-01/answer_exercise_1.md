
1. Indica la diferencia entre el uso de la instrucción CMD y ENV (Dockerfile). 

CMD se usa para ejecutar comandos al momento de iniciar el contenedor.
Por ejemplo, puedes crear un DockerFile con unos cuantos RUN’s para descargar paquetes como por ejemplo el wget y luego podrías declarar una instrucción CMD para que ejecute wget.
Normalmente se utiliza para ejecutar servicios a la hora de arrancar el contendor.
Lo malo del cmd es que solo puedes tener uno por dockerfile porque si tienes varios siempre el siguiente va a sobreescribir el CMD anterior .
También se puede sobreescribir la instrucción CMD a la hora de hacer el RUN del contenedor.
Si por ejemplo ya no quieres hacer dl cmd de wget podrías cambiarlo por otro poniendo la instrucción detrás del RUN pero fuera del DockerFile.
Mucha gente lo confunde con el ENTRYPOINT ya que más o menos tienen la misma finalidad pero la gran diferencia es que no es sobreescribible y si hicieras lo que he mencionado anteriormente de pasarle el comando a ejecutar a la hora de lanzar el contenedor mediante RUN, este fallaría porque espera que lo que le estas pasando sea algún argumento del comando que estás lanzando con ENTRYPOINT. Por ejemplo, en lugar de ejecutar un server podrías declarar Python y a la hora de hacer el RUN podría pasarle como argumento algún fichero .py, de esta manera lo ejecutaría.

ENV se utiliza para establecer variables de entorno que ayuden a controlar tu container.
Se puede utilizar para declarar donde se va a instalar el software que necesito.También podría usarlo en una imagen de base de datos, por ejemplo para definir dos variables el usuario y password de la base datos.
Esto puede ser muy útil para utilizar diferentes variables en función de la configuración que este corriendo como INT PRE o PRO.

Como se puede ver son comandos muy distintos y que no tienen nada que ver entre si.
