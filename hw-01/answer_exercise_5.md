5. Ejercicio 5 PoC elástica search y Kibana.

Para hacer este ejercicio he utilizado el siguiente docker-compose.yml

version: '3.8'

services: 

    elasticsearch:
        
        #Utilizar la imagen de elasticsearch v7.9.3
        
        image: "elasticsearch:7.9.3"

        #Asignar un nombre al contenedor
       
        container_name: elasticsearch_test

        #Definir variables de entorno
        environment:
            - discovery.type=single-node

        #Emplazar el contenedor a la red de elastic

        networks:
            - elastic_network

        #Mapear el puerto host 9200 y 9300 a los mismos del contenedor

        ports:
            - 9200:9200
            - 9300:9300

        healthcheck:
            test: ["CMD", "curl", "-f", "http://localhost:9200"]
            interval: 15s
            timeout: 10s
            retries: 2

    kibana:

        
        #Utilizar la imagen de kibana v7.9.3
        
        image: "kibana:7.9.3"

        #Asignar el nombre al contenedor
        
        container_name: kibana_test

        #Emplazar el contenedor a la red de elastic

        networks:
            - elastic_network

        #Define las siguientes variables de entorno:
        
        environment:
            - ELASTICSEARCH_HOST=elasticsearch
            - ELASTICSEARCH_PORT=9200

        #Mapear el puerto 5601 del host al mismo del contenedor
        ports:
            - 5601:5601

        #Esperar la disponibilidad del servicio elasticsearch
        depends_on:
            elasticsearch:
              condition: service_healthy
    
#Definir la red elastic en modo bridge
networks:
    elastic_network:
        driver: bridge

Pero me esta fallando la comprobación del health para arrancar a posteriori Kibana:
Buscando por internet he visto que la gente suele utilizar lo siguiente :


Así que después de esto he vuelto a probar y … “voila”!















