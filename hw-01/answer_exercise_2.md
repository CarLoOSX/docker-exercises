2. Indica la diferencia entre el uso de la instr  ucción ADD y COPY (Dockerfile). 

Tanto ADD como COPY son instrucciones que tienen instrucciones similares. Ambas te permiten copiar archivos a una imagen de docker.

COPY te permite pasarle una fuente y destino, te permite copiar un directorio de tu maquina host a la imagen de Docker.

En cambio, ADD es mas versátil. Como el anterior soporta una fuente y destino pero puede ser una fuente de un origen que no sea tu maquina host y por otro lado puedes extraer ese fichero en caso de que este comprimido directamente en el destino. Este se podría reemplazar por un RUN con curl.
