
He leido tarde como debía presentarse el/los entregables.

Había hecho un documento en pdf con todos los ejercicios, explicaciones, capturas de pantalla etc.

Entiendo que el formato no penaliza, al final he hecho copy paste de mi documento a los ficheros answer_exercise_*.md pero preferiría si es posible que mirases el documento de pdf que es ahí donde está todo bien explicado y con sus respectivas capturas ya que en los md's no hay más que el texto que he ido copiando del documento original.

Supongo que para la próxima estoy advertido pero ahora mismo no dispongo de más tiempo para dejar unos .md bonitos y que animen a leerselos.

Espero comprensión.

PDF ejercicio --> PHD4 Carlos Martinez.pdf

Output/Logs grabación de terminal mediante el compando script (he grabado todo lo que he hecho hasta el ejercicio 4 más o menos que he dado un exit de más y he dejado de grabar el terminal)  --> ex4.txt 

Gracias.